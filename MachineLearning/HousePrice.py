import numpy as np
from sklearn import datasets
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error,explained_variance_score
from sklearn.utils import shuffle

#Загружаем набор данных с ценами жилья

data=datasets.load_boston()
#Перемешаем данные
X,y=shuffle(data.data,data.target,random_state=7)

#Разобъем данные на обучающий и тестовый набор в соотношение 89/20
num_train=int(0.8*len(X))
X_train,y_train=X[:num_train],y[:num_train]
X_test,y_test=X[num_train:],y[num_train:]


#Создадим и обучим SVM-регрессор,используя линейное ядро
#C-штраф за ошибки обучения.
#epsilon- расхождение между фактическим и предсказаным значением

#Создание регессиной модели на основе SVM
sv_regr=SVR(kernel='linear', C=1.0, epsilon=0.1)
#Обучаем регрессор sv_regr
sv_regr.fit(X_train,y_train)
#Оценим эффективность работы регрессора
y_test_pred=sv_regr.predict(X_test)
mse= mean_squared_error(y_test,y_test_pred)
evs=explained_variance_score(y_test,y_test_pred)
print('\nИтог\n')
print('\nMean squared error\n',round(mse,2))
print('\nExplained variance score\n',round(evs,2))

#Выберемтестовую точкуданных и спрогнозируем для нее результат
test_data=[3.7,0,18.4,1,0.87,5.95,91,2.5052,26,666,20.2,351.34,15.27]
print('\npredict price\n',sv_regr.predict([test_data])[0])