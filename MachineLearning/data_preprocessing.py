import numpy as np
from sklearn import preprocessing

input_data=np.array([[5.1, -2.9, 3.3],
                    [-1.2, 7.8, -6.1],
                    [3.9, 0.4, 2.1],
                    [7.3, -9.9, -4.5]])

#Бинаризация, т.е.преобразуем чиловые значения в булевы
data_binarized = preprocessing.Binarizer(threshold=2.1).transform(input_data)
#print("\nBinarazed data\n",data_binarized)

#Исключение среднего, т.е. из вектора признаков извелкаем среднее значение, чтобы каждый признак в вотдельноти центрировался около нуля №
# исключаем из рассмотрения смещение значеия в векторах признаков

print('\nBefore\n')
print('Mean=', input_data.mean(axis=0))
print('Std deviation=', input_data.std(axis=0))
   #исключаем среднее
data_scaled=preprocessing.scale(input_data)
print('\nAfter\n')
print('Mean=', data_scaled.mean(axis=0))
print('Std deviation=', data_scaled.std(axis=0))

#Масштабирование т.е. ввекторе признаков каждое значение может меняться в некоторых случайных пределах
 #масштабирование MinMax
data_scaled_minmax=preprocessing.MinMaxScaler(feature_range=(0,1))
data_scaled_minmax=data_scaled_minmax.fit_transform(input_data)
print('\nMinMax scaled data\n',data_scaled_minmax)
#Нормализация т.е. изменение значений в векторе признаков, чтобы признаки можо были измерять на одной шкале
data_norma_l1=preprocessing.normalize(input_data, norm='l1')
data_norma_l2=preprocessing.normalize(input_data, norm='l2')
print('\nL1 norma\n',data_norma_l1)
print('\nL2 norma\n',data_norma_l2)