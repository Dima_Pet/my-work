import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import metrics

X=np.loadtxt('data_clastering.txt',delimiter=',')

#Определим количество класетров
num_clasters = 5

#Отобразим входные данные
plt.figure()
plt.scatter(X[:,0], X[:,1],marker='o',facecolors='none',edgecolors='black',s=80)
x_min,x_max=X[:,0].min()-1,X[:,0].max()+1
y_min,y_max=X[:,1].min()-1,X[:,1].max()+1
plt.title('Входные данные')
plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)

#Создадим объект KMeans

kmeans = KMeans(init='k-means++',n_clusters=num_clasters,n_init=30)

#Обучаем
kmeans.fit(X)

#Чтобы визуализировать границы нужно создать сетку и вычислить модель на всех узлах сетки
#Определим шаг сетик
step_size=0.01

#Определим саму сетку
x_vals,y_vals =np.meshgrid(np.arange(x_min,x_max,step_size),np.arange(y_min,y_max,step_size))

#Спрогнозируем результат для всех точек сетки
output = kmeans.predict(np.c_[x_vals.ravel(),y_vals.ravel()])

#отобразим на графике выходные значения и выделим каждую область своим ццветом
output=output.reshape(x_vals.shape)
plt.figure()
plt.clf()
plt.imshow(output,interpolation='nearest',extent=(x_vals.min(),x_vals.max(),y_vals.min(),y_vals.max()),aspect='auto',origin='lower')
#отобразим входные данные на выделеные цветом области
plt.scatter(X[:,0], X[:,1],marker='o',facecolors='none',edgecolors='black',s=80)

#Отобразим центры кластеров
cluster_center=kmeans.cluster_centers_
plt.scatter(cluster_center[:,0],cluster_center[:,1],marker='o',s=210,linewidths=4,color='black',zorder=12,facecolor='black')
plt.title('Границы класетров')
plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)
plt.show()

