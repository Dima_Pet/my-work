import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift,estimate_bandwidth
from itertools import cycle

X=np.loadtxt('data_clastering.txt',delimiter=',')

plt.figure()
plt.scatter(X[:,0], X[:,1],marker='o',facecolors='none',edgecolors='black',s=80)
x_min,x_max=X[:,0].min()-1,X[:,0].max()+1
y_min,y_max=X[:,1].min()-1,X[:,1].max()+1
plt.title('Входные данные')
plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)


#оценим ширину окна

bandwidth_X=estimate_bandwidth(X,quantile=0.1,n_samples=len(X))

#Обучим модель кластеризацийй на основе свдга среднего
# Класстеризация данных методом сдвига среднего
meanshaft_model = MeanShift(bandwidth=bandwidth_X,bin_seeding=True)
meanshaft_model.fit(X)
#Извелечение центров кластеров
cluster_centers=meanshaft_model.cluster_centers_
#Считемкоичество кластеров
labels = meanshaft_model.labels_
num_clasters = len(np.unique(labels))
print('Num clusters: ',num_clasters)
#отображение на графике точек и центров кластеров
plt.figure()
markers='o*xvs'
for i,marker in zip(range(num_clasters),markers):
    #Отображаем точки принадежащие текущему кластеру
    plt.scatter(X[labels==i,0],X[labels==i,1],marker=marker,color='black')
    #Отобразим центр текущео кластера
    cluster_center=cluster_centers[i]
    plt.plot(cluster_center[0],cluster_center[1],marker='o',markerfacecolor='black',markersize=12)
plt.show()