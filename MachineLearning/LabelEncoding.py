import numpy as np
from sklearn import preprocessing

#Предоставление меток входных данных
input_labels=['red','green','black','yellow','white']

#Создадим объект кодирования меток и обучим его
#устанавливаем соотвтествие между метками и числами
encoder=preprocessing.LabelEncoder()
encoder.fit(input_labels)

#Выведем полученные отображения
print('\nLabelmapping\n')
for i,item in enumerate(encoder.classes_):
    print(item,'-->',i)

#Проверим работу кодировзика на случайных метках
test_labels=['green','white','black']
encoder_value=encoder.transform(test_labels)
print('\nLabel\n',test_labels)
print("Encoder values = ",list(encoder_value))