#Если класы не cбалансированы по размеру
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import ExtraTreesClassifier

from utilities import visualize_classifier

#загрузка входных данных
input_file='data_imbalance.txt'
data = np.loadtxt(input_file,delimiter=',')
X,y=data[:,:-1],data[:,-1]

#Разделимвходные данные на основание их меток
class_0=np.array(X[y==0])
class_1=np.array(X[y==1])

#Визуализация входных данных
plt.figure()
plt.scatter(class_0[:,0],class_0[:,1],s=75,facecolor='black',edgecolors='black',linewidths=1,marker='x')
plt.scatter(class_1[:,0],class_1[:,1],s=75,facecolor='black',edgecolors='white',linewidths=1,marker='o')
plt.title('Входные данные')

#разбиваем данные на обучающий и тестовый набор
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25,random_state=5)

#Классификатор на основе дерева принятия решения
params={'n_estimators': 100, 'max_depth': 4, 'random_state': 0, 'class_weight': 'balanced'} #max_depth-максимальная глубина дерева,n_estimators-количетсвозодаваемых деревьев
classifier=ExtraTreesClassifier(**params)
classifier=classifier.fit(X_train, y_train)
visualize_classifier(classifier,X_train,y_train)

#Проверка на тестовом наборе
y_test_data=classifier.predict(X_test)
#visualize_classifier(classifier,X_test,y_test)

#Оценка работы классификатора
class_names=['Class-0','Class-1']
print('\n'+'#'*40)
print('\nClassifier performance on training dataset\n')
print(classification_report(y_train,classifier.predict(X_train),target_names=class_names))
print('\n'+'#'*40)
print('\nClassifier performance on test dataset\n')
print(classification_report(y_test,y_test_data,target_names=class_names))
plt.show()