#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,size,count,first_chet=2,first_nechet=3,pg; 
 MPI_Status status;
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 int* mas = (int*)malloc(size*sizeof(int*));
 pg =size%2;
 //для четных 
 if ((myrank==0)&&(myrank!=first_chet))
 {
 	 MPI_Recv(&mas[size-pg],1,MPI_INT,size-pg,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[size-pg],size-pg);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[size-pg]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[size-pg],count,myrank+2);
 }
 if ((myrank!=first_chet)&&(myrank!=0)&&(myrank!=size-pg)&&(myrank%2==0))
 {
 	 MPI_Recv(&mas[myrank-2],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank-2],myrank-2);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[myrank-2]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[myrank-2],count,myrank+2); 	
 }
 if (myrank==first_chet)
 {
   srand(myrank+1);
   mas[myrank]=rand()%10;
   if (myrank==0)
   {
    MPI_Send(&mas[myrank],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],myrank+2);
    MPI_Recv(&mas[size-pg],1,MPI_INT,size-pg,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[size-pg],size-pg);
   }
   if ((myrank==size-pg))
   {
    MPI_Send(&mas[myrank],1,MPI_INT,0,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],0);
    MPI_Recv(&mas[myrank-2],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank-2],myrank-2);
   }
   if ((myrank!=0)&&(myrank!=size-pg)) 
   {
    MPI_Send(&mas[myrank],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],myrank+2);
    MPI_Recv(&mas[myrank-2],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank-2],myrank-2);
   }
 }
 if ((myrank==size-pg)&&(myrank!=first_chet)&&(myrank%2==0))
  {
  	 MPI_Recv(&mas[myrank-2],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank-2],myrank-2);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[myrank-2]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,0,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[myrank-2],count,0);
  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//для нечетных
 if ((myrank==1)&&(myrank!=first_nechet))
 {
	 MPI_Recv(&mas[myrank+2],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank+2],myrank+2);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[myrank+2]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,size-1-pg,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[myrank+2],count,size-1-pg);
 }
 if ((myrank!=first_nechet)&&(myrank!=1)&&(myrank!=size-1-pg)&&(myrank%2!=0))
 {
 	 MPI_Recv(&mas[myrank+2],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank+2],myrank+2);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[myrank+2]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[myrank+2],count,myrank-2);
 }
 if (myrank==first_nechet)
 {
   srand(myrank+1);
   mas[myrank]=rand()%10;
   if (myrank==1)
   {
    MPI_Send(&mas[myrank],1,MPI_INT,size-1-pg,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],size-1);
    MPI_Recv(&mas[myrank+2],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank+2],myrank+2);
   }
   if (myrank==size-1-pg)
   {
    MPI_Send(&mas[myrank],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],myrank-2);
    MPI_Recv(&mas[1],1,MPI_INT,1,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[1],1);
   }
   if ((myrank!=1)&&(myrank!=size-1)) 
   {
    MPI_Send(&mas[myrank],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD);
    printf("I'm proc %d, send %d to proc %d\n",myrank,mas[myrank],myrank-2);
    MPI_Recv(&mas[myrank+2],1,MPI_INT,myrank+2,99,MPI_COMM_WORLD,&status);
    printf("I'm proc %d recived %d from proc %d\n",myrank,mas[myrank+2],myrank+2);
   }
 }
 if ((myrank==size-1-pg)&&(myrank!=first_nechet)&&(myrank%2!=0))
  {
  	 MPI_Recv(&mas[1],1,MPI_INT,1,99,MPI_COMM_WORLD,&status);
	 printf("I'm proc %d recived %d from proc %d\n",myrank,mas[1],1);
         srand(myrank+1);
         count = rand()%10;
	 mas[myrank]=mas[1]+count;
	 MPI_Send(&mas[myrank],1,MPI_INT,myrank-2,99,MPI_COMM_WORLD);
	 printf("I'm proc %d, send %d+%d to proc %d\n",myrank,mas[1],count,myrank-2);
  }
MPI_Finalize(); 
}



