#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,size,i,j,N=5,max1=0,max=0,Mrank=0;
 MPI_Request request[7]; 
 MPI_Status status[7];
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);
 int** mas = (int**)malloc(size*sizeof(int*));
 int** mas_res = (int**)malloc(size*sizeof(int*));
 for (i=0;i<size;i++)
 {
 mas[i]=(int*)malloc(N*sizeof(int));
 mas_res[i]=(int*)malloc(N*sizeof(int));
 }
 if (myrank!=0)
 {
     srand(myrank+1);
     for (i = 0 ; i < N ; i++)
     {
        mas[myrank-1][i] = 1+rand()%200;
     }

     MPI_Isend(mas[myrank-1],N,MPI_INT,0,99,MPI_COMM_WORLD,&request[myrank-1]);
     MPI_Wait(&request[myrank-1],&status[myrank-1]);
          for (i=0;i<N;i++)
     {
       if (mas[myrank-1][i]>max1)
       {
       max1 = mas[myrank-1][i];
       }
     }   
      printf("i'm proc: %d send array with max: %d \n",myrank,max1);
 }
 else
 {
         for (i = 1 ; i< size; i++)
       {
          MPI_Irecv(mas[i],N,MPI_INT,i,99,MPI_COMM_WORLD,&request[i-1]);
       }
      MPI_Waitall(size-1,request,status);
      for (i=1;i<size;i++)
  {
       for (j=0;j<N;j++)
       {
         if (mas[i][j]>max)
         {
           max = mas[i][j];
           Mrank=i;
         }
       }
  }
     printf("Max elemnt: %d from: %d proc:\n",max,Mrank);
}
MPI_Finalize(); 
}

