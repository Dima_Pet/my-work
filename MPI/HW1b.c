#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>

void main (int argc, char **argv) 
 { 
 int myrank,size,i,j,N=5,max=0,Mrank=0;
 MPI_Request; 
 MPI_Status status;
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 int* mas = (int*)malloc(N*sizeof(int));
 srand(time(NULL));
  if (myrank!=0)
  {
   for (i = 0 ; i < N ; i++)
   {
      mas[i] = rand()%101;
   }
   MPI_Send(mas,N,MPI_INT,0,99,MPI_COMM_WORLD);
 }
 else 
 {
   for (i = 1 ; i< size; i++)
   {
     MPI_Recv(mas,N,MPI_INT,i,99,MPI_COMM_WORLD,&status);
      for (j=0;j<N;j++)
      {
        if (mas[j]>max)
        {
         max = mas[j];
         Mrank=i;
        }
      }
   }
    printf("Max elemnt: %d from: %d proc:\n",max,Mrank);
 }
MPI_Finalize(); 
}