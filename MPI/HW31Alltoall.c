#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,j,size,i,root=0,N=6;
 double sum; 
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 MPI_Status status;
 
 double* mas = (double*)malloc(size*N*sizeof(double*));
 double* rec = (double*)malloc(size*N*sizeof(double*));
 double* suma = (double*)malloc(size*sizeof(double*));
  srand(size+1);
  sum=0;
 for (i=myrank*N;i<(myrank+1)*N;i++)
 {
  mas[i]=1+rand()%10;
  sum=sum+mas[i];
 }
     printf("BEFORE: I'm proc %d with sum: %2.1f \n",myrank,sum);
     MPI_Alltoall(mas,N,MPI_DOUBLE,rec,N,MPI_DOUBLE,MPI_COMM_WORLD);
     sum=0;
    // printf("%2.1f\n",rec[0]);
     for (i=0;i<size*N;i++)
     {
      sum=sum+rec[i];
     }
     printf("AFTER: Sum %2.1f\n",sum);
 MPI_Finalize();
}

