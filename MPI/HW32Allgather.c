#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,j,size,buf,i,N=3,root=0;
 double sum=0; 
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 MPI_Status status;
 srand(myrank+1);
 double* mas = (double*)malloc(N*sizeof(double*));
 double* rec = (double*)malloc(size*N*sizeof(double*));
   for (i=0;i<N;i++)
   {
	   mas[i]=rand()%10;
	   sum=sum+mas[i];
   }
   printf("BEFORE: I'm proc %d with sum: %2.1f \n",myrank,sum);
     MPI_Allgather(mas,N,MPI_DOUBLE,rec,N,MPI_DOUBLE,MPI_COMM_WORLD);
      sum=0;
      for (i=0;i<size*N;i++)
   {
    //printf("%2.1f ",rec[i]);
    sum=sum+rec[i];
   }
   printf("I'm proc %d with sum: %2.1f\n",myrank,sum);
 MPI_Finalize();
}

