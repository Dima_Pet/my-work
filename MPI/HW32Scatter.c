#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,j,size,buf,i,N=3,root=0;
 double sum; 
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 MPI_Status status;
 srand(myrank+1);
 sum=0;
 double* mas = (double*)malloc(size*N*sizeof(double*));
 double* rec = (double*)malloc(N*sizeof(double*));
   for (i=0;i<size*N;i++)
   {
	   mas[i]=rand()%10;
   }
   for (i=myrank*N;i<(myrank+1)*N;i++)
   {
   sum=sum+mas[i];
   }
  printf("BEFORE: Sum:%2.1f for proc %d \n",sum,myrank);
     MPI_Scatter(mas,N,MPI_DOUBLE,rec,N,MPI_DOUBLE,myrank,MPI_COMM_WORLD);
      sum=0;
      for (i=0;i<N;i++)
   {
    sum=sum+rec[i];
   }
   printf("AFTER: I'm proc %d with sum: %2.1f\n",myrank,sum);
 MPI_Finalize();
}