#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

main (int argc, char **argv)
 {
 int myrank,size,i,buf;
 MPI_Request;
 MPI_Status status;
 MPI_Init (&argc, &argv);
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 srand(time(NULL));
  if (myrank==0)
  {
   for (i=1;i<size;i++)
   {
     buf=rand()%101;   
    MPI_Send(&buf,1, MPI_INT, i, 99, MPI_COMM_WORLD); 
    }
 }
 else
 {
  MPI_Recv(&buf,1, MPI_INT, 0, 99, MPI_COMM_WORLD, &status); 
  printf("proc %d received : %d:\n", myrank, buf); 
 }
MPI_Finalize();
}
