#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,size,root=0,N=200,i,j,flag_res=0; 
 double max_norm=0,buf=0,bet,alpha,max=1,eps=0.00001,prod1=0,prod2=0,norm,count,schet=0,t1,t2;
 MPI_Status status;
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 double* x = (double*)malloc(N*sizeof(double));//решение
 double* b = (double*)malloc(N*sizeof(double));//свободные члены
 double* x_prev = (double*)malloc(N*sizeof(double));//предыдущее решение
 double* r = (double*)malloc(N*sizeof(double));//направление 
 double* z = (double*)malloc(N*sizeof(double));//направление 
 double* r_prev = (double*)malloc(N*sizeof(double));//направление 
 double* Buf = (double*)malloc(N*sizeof(double));//шаг спуска
 double* mas = (double*)malloc(N*sizeof(double));//шаг спуска
 double** A = (double**)malloc(N*sizeof(double*));//матрица коэффициентов 
   if (myrank==root)
   {

   for (i=0;i<N;i++)
   {
	   A[i]=(double*)malloc(N*sizeof(double));
   }
                for ( i=0;i<N;i++)
	{
		for ( j = i ; j<N;j++)
		{
			A[i][j]=1.0+rand()%10;
			A[j][i]=A[i][j];
		}
		A[i][i]=A[i][i]*10;
	}
			/* for (int i = 0 ; i<N;i++)
		  {
			 
			  for (int j = 0 ;j<N;j++)
			  {
                            printf("%2.1f ",A[i][j]);
			  }
			  printf("\n");
	      }*/
	for ( i = 0 ; i<N;i++)
	{
		b[i]=1+rand()%10;
		//cout<<b[i]<<endl;
		x_prev[i]=0;
	}
	for( i = 0 ; i<N;i++)
	{
		for ( j = 0 ; j<N;j++)
		{
			buf=buf+A[i][j]*x_prev[j];
		}
		r_prev[i]=b[i]-buf;
		z[i]=r_prev[i];
		if (z[i]==0){printf("z[i]==%f\n",z[i]);}
		//cout<<z[i]<<endl;
	}

	printf("start calculation\n");
	t1=MPI_Wtime();
      while(max>eps)
	  {
		 for ( i = 0 ; i<N;i++)
		  {
			  //надо парралелить.
			  count=0;
			  schet=0;
			  for ( j = 0 ;j<N;j++)
			  {
                                count=count+A[i][j]*z[j];
			  }
			  Buf[i]=count;
			  schet=schet+Buf[i];
			  if (Buf[i]==0){printf("Buf[i]==0\n");}
	          }
		 //printf("schet= %f\n",schet);
		  prod1=0;
		  prod2=0;
		  for ( i = 0 ; i< N;i++)
		  {
                        prod1=prod1+r_prev[i]*r_prev[i];
			prod2=prod2+Buf[i]*z[i];
			//printf("%2.1f\n",Buf[i]*z[i]);
	          }
	         if (prod2==0){printf("prod 2 = %f\n",prod2);break;}
	    //printf("prod 2 = %f\n",prod2);
		  alpha=prod1/prod2;
		  max_norm=0;
		  for ( i = 0 ;i<N;i++)
		  {
			  x[i]=x_prev[i]+alpha*z[i];
			  norm=x[i]-x_prev[i];
			  if (norm<0){norm=norm*(-1);}
			  if (norm>max_norm) {max_norm=norm;}
		  }
		  max=max_norm;
		  for ( i = 0 ;i<N;i++)
		  {
			  r[i]=r_prev[i]-alpha*Buf[i];
		  }
		  prod1=0;
		  prod2=0;
		  for ( i = 0 ;i<N;i++)
		  {
		   prod1=prod1+r[i]*r[i];
		   prod2=prod2+r_prev[i]*r_prev[i];
		  }
		  bet=prod1/prod2;
                      if (prod2==0){printf("prod 2.1 = 0\n");}
		  for ( i = 0 ;i<N;i++)
		  {
			  z[i]=r[i]+bet*z[i];
		  }
		  for( i = 0 ;i<N;i++)
		  {
			  r_prev[i]=r[i];
			  x_prev[i]=x[i];
		  }
	  }
	  t2=MPI_Wtime();
	  printf("Time %f witn %d element\n",t2-t1,N);
	  printf("%f\n",max);
		/*for (i=0;i<N;i++)
		{
			printf("%f\n",x_prev[i]);
		}*/
		for (i=0;i<N;i++)
		{
		  buf=0;
		  for (j=0;j<N;j++)
		  {
		    buf=buf+A[i][j]*x_prev[j];
		  }
		  //printf("%f\n",buf-b[i]);
		  if ((buf-b[i])>0.01){flag_res=1;}
		}
		if (flag_res==0){printf("CORRECT!!!\n");}
		else {printf("ERROR!!!\n");}
		
   }
MPI_Finalize(); 
}

