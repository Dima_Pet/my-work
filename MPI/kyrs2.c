#include "mpi.h" 
#include <stdio.h>
#include <stdlib.h>
main (int argc, char **argv) 
 { 
 int myrank,size,root=0,N=1600,i,j,k,count,flag,flag_res; 
 double max_norm=0.0,buf=0.0,bet,alpha,prod1,prod2,norm,max=1,eps=0.00001,t1,t2,check,t3,t4,t5,t6,count_t=0,count_pt=0,count_r=0;
 MPI_Status status;
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
 double* x = (double*)malloc(N*sizeof(double));//решение
 double* z = (double*)malloc(N*sizeof(double));
 double* b = (double*)malloc(N*sizeof(double));//свободные члены
 double* x_prev = (double*)malloc(N*sizeof(double));//предыдущее решение
 double* r = (double*)malloc(N*sizeof(double));//направление 
 double* r_prev = (double*)malloc(N*sizeof(double));//направление 
 int* Count = (int*)malloc((size-1)*sizeof(int));//сколько в каждом процессоре 
 double* Buf = (double*)malloc(N*sizeof(double));//шаг спуска
 double* recv = (double*)malloc(N*sizeof(double));//
 
 double** A = (double**)malloc(N*sizeof(double*));//матрица коэффициентов 
 flag=1;
 flag_res=0;
   for (i=0;i<N;i++)
   {
	   A[i]=(double*)malloc(N*sizeof(double));
   } 
 
   
   if (myrank!=root)
   { 
      MPI_Recv(&count,1,MPI_INT,root,99,MPI_COMM_WORLD,&status);
        //printf("I'm proc %d with count = %d\n",myrank,count);
          double** bufer = (double**)malloc(count*sizeof(double*));//матрица коэффициентов для каждого процессора
          for (i=0;i<count;i++)
           {
	     bufer[i]=(double*)malloc(N*sizeof(double));
           } 

           double* send = (double*)malloc(count*sizeof(double));//массив для отправки
           for (i=0;i<count;i++)
           {
             MPI_Recv(bufer[i],N,MPI_DOUBLE,root,99,MPI_COMM_WORLD,&status);
        
           }
           double t7,t8;
    
	   while(flag==1)
	   {
	       MPI_Recv(&flag,1,MPI_INT,root,99,MPI_COMM_WORLD,&status);
	          if (flag==1)
	              {
	                 MPI_Recv(z,N,MPI_DOUBLE,root,99,MPI_COMM_WORLD,&status);
	                 t7=MPI_Wtime();
	                  for (i=0;i<count;i++)
	                  {
	                  send[i]=0.0;
	                    for (j=0;j<N;j++)
	                    {
	                	   send[i]+=bufer[i][j]*z[j];
	                    }
	                   }
	                   t8=MPI_Wtime();
	                   count_pt+=(t8-t7);
	                      t5=MPI_Wtime();
	                      MPI_Send(send,count,MPI_DOUBLE,root,99,MPI_COMM_WORLD);
	                      t6=MPI_Wtime();
	                      count_r+=(t6-t5);
	                      t5=0;
	                      t6=0;
	              }
	              
	   }
	   printf("I'm proc %d with calculation time %f and sendtime %f\n",myrank,count_r);
   }


   if (myrank==root)
   {
   srand(5);
	   //////////////////////////////ЗАПОЛНЕНИЕ//////////////////////////////////
	   for (i=0;i<N;i++)
	   {
		   for (j=i;j<N;j++)
		   {
			   A[i][j]=1.0+rand()%10;
			   A[j][i]=A[i][j];  
			   //printf("%2.1f ",A[i][j]);
		   }
		   //printf("\n");
		   A[i][i]=A[i][i]*10;
	    }
	    //printf("b\n");
	    for (i=0;i<N;i++)
	    {
		   b[i]=1+rand()%10;
		   //printf("%f\n",b[i]);
		   x_prev[i]=0;
	    }
	   
        for (i=0;i<N;i++)
		{
			for (j=0;j<N;j++)
			{
				buf=buf+A[i][j]*x_prev[j];
			}
			r_prev[i]=b[i]-buf;
			z[i]=r_prev[i];
			//printf("%f\n",z[i]);
		}
		//считем сколько строк надо отправить каждму процу
		for (i=1;i<size;i++)
		{
		 Count[i]=0;
		 for (j=(i-1);j<N;j+=(size-1))
		 {
		 Count[i]++;
		 }
		 MPI_Send(&Count[i],1,MPI_INT,i,99,MPI_COMM_WORLD);
		}

		//отправляем соответсвующие строки.

		for (i=1;i<size;i++)
		{
		 for (j=i-1;j<N;j+=size-1)
		 {
		     MPI_Send(A[j],N,MPI_DOUBLE,i,99,MPI_COMM_WORLD);
		 }
		}
		printf("start calculation\n");
		//////////////////////////////////////СЧЕТ////////////////////////////////////
		count=0;
		double tf,count_p=0,count_pt=0,tc1,tc2,count_tc=0;
		t1=MPI_Wtime();
		while(max>eps)
		{
		count++;
			////////////////////////распараллеливание/////////////
	 		t3=MPI_Wtime();
	  		for (i=1;i<size;i++)
			{ 
			  MPI_Send(&flag,1,MPI_INT,i,99,MPI_COMM_WORLD);
			  MPI_Send(z,N,MPI_DOUBLE,i,99,MPI_COMM_WORLD);
			}
			t4=MPI_Wtime();
			count_t+=t4-t3;

			for (i=1;i<size;i++)
			{
			t5=MPI_Wtime();
			 MPI_Recv(recv,Count[i],MPI_DOUBLE,i,99,MPI_COMM_WORLD,&status);
			t6=MPI_Wtime();
			count_pt+=(t6-t5);
			 for (j=i-1,k=0;j<N,k<Count[i];j+=size-1,k++)
			 {
			 Buf[j]=recv[k];
			 }
			}
			tf=MPI_Wtime();
			count_p+=(tf-t3);
			////////////////////////////////////////////конец распараллеливания////////////////////////
			tc1=MPI_Wtime();
			prod1=0.0;
			prod2=0.0;
		  for (i = 0 ; i<N ;i++)
		  {
                        prod1+=r_prev[i]*r_prev[i];
			prod2+=Buf[i]*z[i];
	          }
	      if (prod2==0){printf("prod2 = %f\n",prod2);break;}
		  alpha=prod1/prod2;
		  max_norm=0.0;
		  for (i = 0 ;i<N;i++)
		  {
			  x[i]=x_prev[i]+alpha*z[i];
			  norm=x[i]-x_prev[i];
			  if (norm<0){norm*=-1;}
			  if (norm>max_norm){max_norm=norm;}
		  }
		  max=max_norm;
		  for ( i = 0 ;i<N;i++)
		  {
			  r[i]=r_prev[i]-alpha*Buf[i];
		  }
		  prod1=0.0;
		  prod2=0.0;
		  for ( i = 0 ;i<N;i++)
		  {
		   prod1+=r[i]*r[i];
		   prod2+=r_prev[i]*r_prev[i];
		  }
		  bet=prod1/prod2;

		  for ( i = 0 ;i<N;i++)
		  {
			  z[i]=r[i]+bet*z[i];
		  }
		  for( i = 0 ;i<N;i++)
		  {
			  r_prev[i]=r[i];
			  x_prev[i]=x[i];
		  }
		  tc2=MPI_Wtime();
		  count_tc+=(tc2-tc1);
		}
		t2=MPI_Wtime();
		printf("Time %f sec with %d element and %d process; Iterations = %d\n",t2-t1,N,size-1,count);
		printf("Parallel calculation time (in root): %f\n",count_p);
		printf("        Sendtime from root: %f\n",count_t);
		printf("        Recivtime to root: %f\n",count_pt);
		printf("        Total parallel time: %f\n",count_t+count_pt);
		printf("Calculationtime in root: %f\n",count_tc);
	        printf("Total time in root: %f\n",count_t+count_pt+count_tc);
		
		flag=0;
		for (i=1;i<size;i++)
		{
			MPI_Send(&flag,1,MPI_INT,i,99,MPI_COMM_WORLD);
		}
		
		
		//printf("res\n");
		/*for (i=0;i<N;i++)
		{
			printf("%f\n",x_prev[i]);
		}*/
		//printf("check\n");
		/*for (i=0;i<N;i++)
		{
		  buf=0.0;
		  for (j=0;j<N;j++)
		  {
		    buf=buf+A[i][j]*x_prev[j];
		  }
		  check=buf-b[i];
		  if (check<0){check*=-1;}
		  if (check>0.1)
		  {
		  flag_res=1;
		  //printf("%f\n",check);
		  }
		}
		if (flag_res==0){printf("CORRECT!!!\n");}
		else {printf("ERROR!!!\n");}*/
   }
MPI_Finalize(); 
}

