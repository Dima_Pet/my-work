<?php
  session_start();
  require_once 'connection.php';
  if (!mysqli_set_charset($con,'utf8')){
    echo 'Error code';
  }

  $FirstName=htmlspecialchars($_POST['FirstName']);
  $LastName=htmlspecialchars($_POST['LastName']);
  $email=htmlspecialchars($_POST['email']);
  $password=htmlspecialchars($_POST['password']);
  $password_confirm=htmlspecialchars($_POST['password_confirm']);
  //Проверяем правильность ввода пароля
  if ($password!=$password_confirm)
  {
    $_SESSION['message']='Пароли не совпадают!';
    header('Location:../Registration.php');
    exit();
  }
  else
  {
  //проверяем, что этот пользователь еще не зарегестрированн
  $user_not_exist = mysqli_prepare($con, "SELECT * FROM users WHERE email=?;");
  mysqli_stmt_bind_param($user_not_exist,'s',$email);
  mysqli_stmt_execute($user_not_exist);
  if (mysqli_stmt_fetch($user_not_exist)){
     $_SESSION['message']='Пользователь с таким email уже существует';
     header('Location:../Registration.php');
    exit();
  }
  //добавляем пользователя в базу данных
  else{
    $reg_user=mysqli_prepare($con,"INSERT INTO `users`  ( `FirstName`, `LastName`, `email`, `password` )VALUES (?, ?, ?, ?);");
    mysqli_stmt_bind_param($reg_user,'ssss',$FirstName,$LastName,$email,$password);
    mysqli_stmt_execute($reg_user);
    if (mysqli_stmt_affected_rows($reg_user)>0)
    {
       $_SESSION['message']='Вы зарегестрированны!';
       header('Location:../LogIn.php');
     }
  }
}
  //закрываем базу данных
   mysqli_close($con);
 ?>
